<?php
// HTTP
define('HTTP_SERVER', 'http://www.aneytis.kiev.ua/admin/');
define('HTTP_CATALOG', 'http://www.aneytis.kiev.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://www.aneytis.kiev.ua/admin/');
define('HTTPS_CATALOG', 'http://www.aneytis.kiev.ua/');

// DIR
define('DIR_APPLICATION', '/home/aneytis/aneytis.kiev.ua/www/admin/');
define('DIR_SYSTEM', '/home/aneytis/aneytis.kiev.ua/www/system/');
define('DIR_IMAGE', '/home/aneytis/aneytis.kiev.ua/www/image/');
define('DIR_STORAGE', '/home/aneytis/aneytis.kiev.ua/storage/');
define('DIR_CATALOG', '/home/aneytis/aneytis.kiev.ua/www/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'aneytis.mysql.tools');
define('DB_USERNAME', 'aneytis_db');
define('DB_PASSWORD', 'nm148r9l');
define('DB_DATABASE', 'aneytis_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
define('OPENCARTFORUM_SERVER', 'https://opencartforum.com/');
